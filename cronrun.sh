#!/bin/bash

LOGDIR=/var/log/trb/
LOGF=${LOGDIR}/$(date +%y%m).addani.log
LOGTMP=${LOGDIR}/tmp.addani.log

${0%/*}/add_anime_rec.rb -y > $LOGTMP
cat $LOGTMP >> $LOGF

# 失敗を検出しメール送信
if grep Error: $LOGTMP ; then
  ${0%/*}/send_log_mail.rb ${0%/*}/conf.yml $LOGTMP
  exit 1
else
  exit 0
fi

