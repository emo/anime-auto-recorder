#!/bin/ruby -Ku
# アニメのタイトルリストを生成する

require 'mysql2'
require 'yaml'


class AnimeList
  
  def initialize
    # config読み込み
    @conf = YAML.load_file(File.dirname(__FILE__) + '/conf.yml')
    @client = Mysql2::Client.new(@conf[:client])
    @rep_list = @conf[:replace].collect{|v| [Regexp.new(v[0]), v[1]] }
    # blacklist生成
    bl_files = Dir[File.dirname(__FILE__) + '/blacklist/*.yml']
    @blacklist = []
    bl_files.each{|f|
      @blacklist.concat(YAML.load_file(f) || [])
    }
    # 小文字化
    @blacklist.each{ |v|
      if String === v then
        v.downcase!
      elsif (Hash === v) && (String === v['title'])
        v['title'].downcase!
      end
    }
  end
  
  # blacklistに一致するか確かめる. titleは置換済みとする.
  def blacklist? title, ch
    @blacklist.find{|v|
      if Hash === v # Hash形式の場合
        (v['title'] === title) && # タイトル一致 かつ 
        ( # チャンネル情報無し か NG指定チャンネル
          (v['channel'] == nil) || v['channel'].find{|c| c == ch}
        )
        # v['title']がRegexpの場合は===の右辺でなければRegexpマッチができない
      else # タイトルのみの場合
        title.index(v)
      end
    }
  end
  
  # => [record hash]
  # recordの['title']はconfのreplateで置換されたものが入る
  # 元のタイトルは['full_title']に入る
  # opt[:raw] = true でblacklistチェックをしない
  def get_anime_list opt = {}
    ch = @conf[:channel].collect{|v| "'#{v}'" }.join(',')
    q = "select * from Recorder_programTbl where category_id=8 and channel in (#{ch}) and starttime > now()"
    result = []
    # リスト取得
    @client.query(q).each{|v|
      v['full_title'] = v['title'].dup
      # refarenceが入るため,以降の破壊的操作は全てv['title']にも作用する
      t = v['title']
      # 全角→半角,小文字化
      t.tr!('０-９ａ-ｚＡ-Ｚ！　', '0-9a-zA-Z! ')
      t.downcase!
      # Regex変換適用
      t2 = t.dup # 空になった場合のバックアップ
      @rep_list.each{|r,v| t.gsub!(r,v) }
      # Regex変換の結果が空なら適用前を利用する
      v['title'] = t2 if(t.empty?)
      # blacklistなら追加しない
      result << v unless !opt[:raw] && blacklist?(v['title'], v['channel'])
    }
    return result
  end
  
  # blacklist形式のリスト作成
  def get_blacklist_format
    # 録画対象取得
    al = get_anime_list
    
    # 放送チャンネル取得
    res = Hash.new{|h,k| h[k] = [] }
    al.each{|rcd| res[rcd['title']] << rcd['channel'] }
    res = res.collect{|k,v| {'title'=>k, 'channel'=>v.uniq.sort} }

    # ホワイトリスト読み込み
    wl = []
    Dir[File.dirname(__FILE__) + '/whitelist/*.yml'].each{|wlf|
      wl += YAML.load_file(wlf)
    }

    # ホワイトリスト適用
    res.delete_if{|h|
      wl.find{|wle| h['title'].index(wle) }
        # .tap{|b| p h if b } # 適用された項目を表示
    }
    return res
  end

end


# ファイルそのまま実行でbase blacklist YAML作成
if __FILE__ == $0
  require 'optparse'
  opt = OptionParser.new
  # -t で対象タイトル置換結果を確認する
  opt.on('-t', "function check: title replace") {|v|
    AnimeList.new().get_anime_list(:raw => true).each{ |v|
      puts "#{v['full_title']},#{v['title']}"
    }
    exit
  }
  opt.parse!(ARGV)

  puts YAML.dump(AnimeList.new().get_blacklist_format())
end
