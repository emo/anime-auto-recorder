#!/bin/bash

LOGDIR=/var/log/trb/
LOGF=${LOGDIR}/$(date +%y%m).rerec.log

{
  ${0%/*}/cancel_all.rb -y;
  ${0%/*}/../add_anime_rec.rb -y
} | tee -a $LOGF
