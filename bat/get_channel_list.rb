#!/bin/ruby
# チャンネル一覧を取得する
require 'mysql2'
require 'yaml'
require 'kconv'

class ChannelList
  def initialize
    conf = YAML.load_file(File.dirname(__FILE__) + '/../conf.yml')
    @client = Mysql2::Client.new(conf[:client])
  end

  def getlist
    q = "select channel,name,channel_disc from Recorder_channelTbl"
    @client.query(q).collect{|v| [v['channel'],v['name'],v['channel_disc']].join(",") }
  end

end

if __FILE__ == $0
  puts ChannelList.new.getlist.join("\n")
end

