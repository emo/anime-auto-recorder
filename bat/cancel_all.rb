#!/bin/ruby -Ku

require_relative '../add_anime_rec.rb'
require 'set'
require 'open-uri'

# AnimeListに録画対象取得メソッドを追加
class AnimeList
  # 録画予約したリストを返す.
  # => [{ :id=>epgrec_program_id, :title=>f}]
  def get_reserve_list
    # 予約される番組を取得
    al = get_anime_list(:raw => true)
    # 予約一覧作成. program_id => id のHashに変換.
    q  = 'select id,program_id from Recorder_reserveTbl'
    reserves = {}
    @client.query(q).each{|r| reserves[r['program_id']] = r['id'] }
    # 番組リストを作成し返す. また,予約IDもHashに追加する.
    return al.find_all{|v| v['reserve_id'] = reserves[v['id']] }
  end
  
  # 予約解除実行
  def cancel_reserve id
    puts open(@conf[:cancel_reserve_url] % id){|f| f.read }
  end
  
end

if __FILE__ == $0
  al = AnimeList.new()
  recl = al.get_reserve_list()
  if recl.empty?()
    puts "<empty>"
    exit(0)
  end

  exit(0) unless check_prompt(recl)
  
  # 予約解除実行
  recl.each{|r| al.cancel_reserve(r['reserve_id']) }
  
end
