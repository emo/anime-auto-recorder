#!/bin/ruby -Ku
=begin

エラー時にlogをメールで送る。
sendmailの設定をするより作った方が楽そうだったので別に作成。
usage: send_log_mail.rb <config YAML file> <log file>

=end

require 'mail'
require 'yaml'

def send_log_mail(conf, log)
  # ファイル読み込み
  conf = YAML.load_file(conf)[:mail]
  log = IO.read(log)

  # Mail送信
  Mail.new do
    # 送信設定
    [:from, :to, :subject].each do |s|
      __send__(s, conf[s])
    end
    # 本文設定
    body(log)
    # 送信設定(SMTP)
    delivery_method(:smtp, conf[:smtp])
    # 送信
    deliver()
  end
end

if __FILE__ == $0 then
  send_log_mail(*ARGV)
end

=begin
# sample

smtp = Net::SMTP.new("smtp-mail.outlook.com", 587)
smtp.enable_starttls()
smtp.start() do
  from='emo-webs@outlook.com'
  to='emo-webs@outlook.com'
  header="From: fromfrom<#{from}>\nTo: toto<#{to}>\nSubject: Test\n"
  smtp.send_message(<<-EndOfMail, from, to)
From: Your Name <from@example.com>
To: Dest Address <to@example.net>
Subject: test mail
Date: Sat, 23 Jun 2001 16:26:43 +0900
Message-Id: <unique.message.id.string@yourhost.example.com>

This is a test mail.
  EndOfMail
}  
end

=end
