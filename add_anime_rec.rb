#!/bin/ruby -Ku

require_relative 'get_anime_list.rb'
require 'set'
require 'open-uri'

# AnimeListに録画対象取得メソッドを追加
class AnimeList
  # 録画予約IDのsetを取得
  def get_reserver_id_set
    q = 'SELECT program_id FROM Recorder_reserveTbl'
    ids = @client.query(q).collect{|r| r['program_id'] }
    return Set.new(ids)
  end

  # 録画対象のリストを返す.
  # => [{ :id=>epgrec_program_id, :title=>f}]
  def get_rec_list
    al = get_anime_list()
    # 予約されている分を削除
    rids = get_reserver_id_set()
    al.reject!{|v| rids.include?(v['id']) }
    return al
  end
  
  # 予約実行
  # => String
  # 成功時 <id>:<チューナー？>:<予約ID?>:[0|1]:0
  # 失敗時 Error:重複により予約できません
  def reserve_rec id
    open(@conf[:reserve_url] % id){|f| f.read }
  end
  
end

# 確認prompt
def check_prompt recl
  return true if ARGV.find{|v| v == "-y" }
  recl.each{|v| puts "#{v['id']}: #{v['full_title']} (#{v['channel']})" }
  print "ok ? [y/n] > "
  return STDIN.gets.to_s.chomp.match(/^y$/i)
end

if __FILE__ == $0
  al = AnimeList.new()
  recl = al.get_rec_list()
  if recl.empty?()
    puts "<empty>"
    exit(0)
  end
  exit(0) unless check_prompt(recl)
  
  # 予約実行
  recl.each{|r|
    info = "#{r['id']}: #{r['full_title']} (#{r['channel']})"
    print info
    res = al.reserve_rec(r['id'])
    puts " => #{res}"
  }
  
end
